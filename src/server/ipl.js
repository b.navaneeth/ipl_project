const csv=require('csvtojson');
const fs = require('fs');
const economicalBowlers = require("./ipl_functions/economicalBowlers.js");
const seasonPlayed = require("./ipl_functions/seasonPlayed.js");
const winTeam = require("./ipl_functions/winTeam.js");
const extraRuns = require("./ipl_functions/extraRuns.js");
const tossAndMatch = require("./ipl_functions/toss&Match.js");
const manOfMatch = require("./ipl_functions/manOfMatch.js");
const taken = require("./ipl_functions/taken.js");
const superOver = require("./ipl_functions/superover.js");
const strikeRate = require("./ipl_functions/batsmanStrikeRate.js");
async function n(){
let matches =await csv().fromFile('../data/matches.csv');
let deliveries = await csv().fromFile('../data/deliveries.csv');
let sortEco=economicalBowlers(matches,deliveries);


//Number of matches played per year for all the years in IPL.

fs.writeFile('../public/output/economicalBowlers.json',JSON.stringify(sortEco),(err) => {
    if (err) {
        throw err;
    }
});


//Number of matches won per team per year in IPL.

let played=seasonPlayed(matches);
fs.writeFile('../public/output/seasonPlayed.json',JSON.stringify(played),(err) => {
    if (err) {
        throw err;
    }
});


//Extra runs conceded per team in the year 2016.

let win=winTeam(matches);
fs.writeFile('../public/output/winPerTeam.json',JSON.stringify(win),(err) => {
    if (err) {
        throw err;
    }
});


//Top 10 economical bowlers in the year 2015.

let e= extraRuns(matches);
fs.writeFile('../public/output/extraRuns.json',JSON.stringify(e),(err) => {
    if (err) {
        throw err;
    }
});


//Find the number of times each team won the toss and also won the match.

let t=tossAndMatch(matches);
fs.writeFile('../public/output/tossAndMatchWinPerTeam.json',JSON.stringify(t),(err) => {
    if (err) {
        throw err;
    }
});


//Find a player who has won the highest number of *Player of the Match* awards for each season.

let mom=manOfMatch(matches);
fs.writeFile('../public/output/maxManOfMatchPerYear.json',JSON.stringify(mom),(err) => {
    if (err) {
        throw err;
    }
});


//Find the highest number of times one player has been dismissed by another player.

let hd=taken(deliveries);
fs.writeFile('../public/output/maxdismissed.json',JSON.stringify(hd),(err) => {
    if (err) {
        throw err;
    }
});


//Find the bowler with the best economy in super overs.

let so=superOver(deliveries);
fs.writeFile('../public/output/economicalBowlersInSuperOver.json',JSON.stringify(so),(err) => {
    if (err) {
        throw err;
    }
});


//Find the strike rate of a batsman for each season.

let batsman = strikeRate(matches,deliveries);
fs.writeFile('../public/output/batsmanStrikeRate.json',JSON.stringify(batsman),(err) => {
    if (err) {
        throw err;
    }
});
}
n();
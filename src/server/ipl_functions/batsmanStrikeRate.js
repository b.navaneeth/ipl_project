function get(matches){
    let a={};
    for (let data in matches){
        if(!(a[matches[data].season])){
            a[matches[data].season]=matches[data].id;
        }
    }
    return a;
}
function getSeason(a,id){
    let p=2017;
    for (let d in a){
        if(a[d]>id){
            return parseInt( p);
        }
        else{
            p=d;
        }
    }
    return 2016;
}
module.exports=function (matches,deliveries){
   let a= get(matches);
   let player={};
   let g_id=0;
   let year=0;
   for (let d in deliveries){
       data=deliveries[d];
       if(!(g_id==data.match_id)){
           g_id=parseInt( data.match_id);
           year=getSeason(a,g_id);
       }
       if(!player[data.batsman]){
           player[data.batsman]={};
           player[data.batsman][year]={ball:1,runs:parseInt(data.batsman_runs)};
       }
       else{
           if(!player[data.batsman][year]){
            player[data.batsman][year]={ball:1,runs:parseInt(data.batsman_runs)};
           }
           else{
            ++player[data.batsman][year]['ball'];
            player[data.batsman][year]['runs']+=parseInt(data.batsman_runs);
           }
       }
   }
   let strikeRate={};
   for (let p in player){
       strikeRate[p]={};
       for(let y in player[p]){
           strikeRate[p][y]=((player[p][y]['runs']/player[p][y]['ball'])*100).toPrecision(5);
       }
   }
return strikeRate;
}
module.exports=function (matches){
    t={};
    for (let d in matches){
        data=matches[d];
        if(data.toss_winner==data.winner){
            if(!t[data.winner]){
                t[data.winner]=1;
            }
            else{
                ++ t[data.winner];
            }
        }
    }
    return t;
}
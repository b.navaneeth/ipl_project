function getdata(matches,deliveries){
    let first=0;
    let last=0;
    let prev=0;
    for (let match in matches){
        if((prev!=2015)&&(matches[match].season==2015)){
            first=parseInt(matches[match].id);
        }
        if((prev==2015)&&(matches[match].season!=2015)){
            last =parseInt( matches[match].id-1);
        }
     prev = matches[match].season;
       
    }
    let deliveries1=deliveries.filter((data)=>data.match_id>=first && data.match_id<=last);
    return deliveries1;
}
module.exports=function (matches,deliveries){
let c={};
deliveries=getdata(matches,deliveries);
deliveries.forEach((a)=>{
    if(!c[a.bowler]){
        c[a.bowler]={ball:1,runs: parseInt(a.total_runs)};
    }
    else{
        ++c[a.bowler]['ball'];
        c[a.bowler]['runs']=c[a.bowler]['runs']+parseInt(a.total_runs);
    }
})
let eco_c=[];
for (let data in c){
    eco_c.push([data,(c[data]['runs']/(c[data]['ball']/6)).toPrecision(3)]);
}
let sortE=eco_c.sort((a,b)=>{
    return a[1] - b[1];
}).slice(0, 10);
let sortEco={};
sortE.forEach((data)=>{
    sortEco[data[0]]=data[1];
})
return sortEco;
}
module.exports=function (matches){
    mom={};
    for (let d in matches){
        data=matches[d];
        if(!mom[data.season]){
            mom[data.season]={};
            mom[data.season][data.player_of_match]=1;
        }
        else{
            if(!mom[data.season][data.player_of_match]){
                mom[data.season][data.player_of_match]=1;
            }
            else{
                ++mom[data.season][data.player_of_match];
            }
        }
    }
    let yt={}
        for (let f1 in mom){
            let ma;
            let max=0;
            for(let f2 in mom[f1]){
                if(mom[f1][f2]>max){
                    max=mom[f1][f2];
                    ma=f2;
                }
            }
            yt[f1]=ma;
        }
        return yt;
    }
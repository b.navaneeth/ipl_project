module.exports=function (matches){
    played={};
    for (let d in matches){
        data=matches[d];
        if(!played[data.season]){
            played[data.season]=1;
        }
        else{
            ++ played[data.season];
        }
    };
    return played;
}
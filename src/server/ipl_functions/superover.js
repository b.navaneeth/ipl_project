module.exports=function (deliveries){
    so={};
    for (let d in deliveries){
        data=deliveries[d];
    if(!(data.is_super_over==0)){
        if(!so[data.bowler]){
            so[data.bowler]={ball:1, runs:parseInt(data.total_runs)};
        }
        else{
            ++so[data.bowler]['ball'];
            so[data.bowler]['runs']=so[data.bowler]['runs']+parseInt(data.total_runs);
        }}
}

let eco_c=[];
for (let data in so){
    eco_c.push([data,(so[data]['runs']/(so[data]['ball']/6)).toPrecision(3)]);
}
let sortE=eco_c.sort((a,b)=>{
    return a[1] - b[1];
});
let sortEco={};
sortEco[sortE[0][0]]=sortE[0][1];
return sortEco;
}
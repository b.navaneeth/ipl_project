module.exports=function (deliveries){
    t={};
    for (let d in deliveries){
        data=deliveries[d];
        if(!(data.player_dismissed =='')){
            let bo=[data.player_dismissed,data.bowler];
            if(!t[bo]){
                t[bo]=1;
            }
            else{
                ++ t[bo];
            }
        }
    }
    let ma;
    let max=0;
    for(let f1 in t){
        if(t[f1]>max){
            max=t[f1];
            ma=f1;
        }
    }
    s={};
    s[ma]=t[ma];
    return s;
}
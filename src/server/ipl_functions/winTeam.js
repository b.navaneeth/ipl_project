module.exports=function (matches){
    win={};
    for (let d in matches){
        data=matches[d];
        if(!win[data.season]){
            win[data.season]={};
            win[data.season][data.winner]=1;
        }
        else{
            if(!win[data.season][data.winner]){
                win[data.season][data.winner]=1;
            }
            else{
                ++win[data.season][data.winner];
            }
        }
    }
    return win;
}